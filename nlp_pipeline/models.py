import torch
import torch.nn as nn
from transformers import AutoModel

class SOCClassifier(nn.Module):
    def __init__(self,model, num_soc_labels):
        super(SOCClassifier, self).__init__()
        self.model = model
        self.drop = nn.Dropout(0.3)
        self.out = nn.Linear(self.model.config.hidden_size, num_soc_labels)
        self.layer_norm = nn.LayerNorm(self.model.config.hidden_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input_ids, attention_mask, token_type_ids):
        model = self.model(input_ids=input_ids,attention_mask=attention_mask, token_type_ids=token_type_ids, return_dict=True)
        output = self.drop(model["pooler_output"])
        output = self.layer_norm(output)
        output_logits = self.out(output)
        soc_output = self.softmax(output_logits)
        return model , soc_output, output_logits




class PTClassifier(nn.Module):
    def __init__(self, model, num_pt_labels, num_soc_labels):
        super(PTClassifier, self).__init__()
        self.model =model
        self.drop = nn.Dropout(0.3)
        self.hidden_layer = nn.Linear(self.model.config.hidden_size + num_soc_labels, self.model.config.hidden_size*2)
        self.hidden_layer_2 = nn.Linear(self.model.config.hidden_size*2, self.model.config.hidden_size*4)
        self.out = nn.Linear(self.model.config.hidden_size *4, num_pt_labels)
        self.layer_norm_1 = nn.LayerNorm(self.model.config.hidden_size*2)
        self.layer_norm_2 = nn.LayerNorm(self.model.config.hidden_size*4)
        self.softmax = nn.LogSoftmax(dim=1)
    def forward(self, input_ids, attention_mask, token_type_ids, soc_logits):
        model = self.model(input_ids=input_ids,attention_mask=attention_mask, token_type_ids=token_type_ids, return_dict=True)
        output = self.drop(model["pooler_output"])
        output = torch.cat([output, soc_logits], dim = -1)
        output = self.hidden_layer(output)
        output = self.layer_norm_1(output)
        output = torch.relu(output)
        output = self.hidden_layer_2(output)
        output = self.layer_norm_2(output)
        output = torch.relu(output)
        output_logits = self.out(output)
        pt_output = self.softmax(output_logits)
        return model , pt_output,  output_logits
    

class LLTClassifier(nn.Module):
    def __init__(self,model, num_pt_labels, num_soc_labels, num_llt_labels):
        super(LLTClassifier, self).__init__()
        self.model = model
        self.drop = nn.Dropout(0.3)
        self.hidden_layer = nn.Linear(self.model.config.hidden_size + num_soc_labels + num_pt_labels, self.model.config.hidden_size*2)
        self.hidden_layer_2 = nn.Linear(self.model.config.hidden_size*2, self.model.config.hidden_size*4)

        self.out = nn.Linear(self.model.config.hidden_size *4, num_llt_labels)
        self.layer_norm_1 = nn.LayerNorm(self.model.config.hidden_size*2)
        self.layer_norm_2 = nn.LayerNorm(self.model.config.hidden_size*4)

        self.softmax = nn.LogSoftmax(dim=1)
        
    def forward(self, input_ids, attention_mask, token_type_ids, soc_logits, pt_logits):
        model = self.model(input_ids=input_ids,attention_mask=attention_mask, token_type_ids=token_type_ids, return_dict=True)
        # last_hidden_state = model["last_hidden_state"] # shape: (batch_size, seq_len, hidden_size)
        # cls_hidden_state = last_hidden_state[:, 0, :] # shape: (batch_size, hidden_size) 
        # cls_hidden_state = self.drop(cls_hidden_state)
        # output = torch.cat([cls_hidden_state, soc_logits, pt_logits], dim=-1)
        output = self.drop(model["pooler_output"])
        output = torch.cat([output, soc_logits, pt_logits], dim = -1)
        output = self.hidden_layer(output)
        output = self.layer_norm_1(output)
        output = torch.relu(output)
        output = self.hidden_layer_2(output)
        output = self.layer_norm_2(output)
        output = torch.relu(output)
        output_logits = self.out(output)
        llt_output = self.softmax(output_logits)
        return model , llt_output
    


