import pandas as pd
import re
from sklearn.preprocessing import LabelEncoder
import numpy as np



def split_words(text):
    words =  [word for word in text.split() if len(word)<4 ]
    return words if words else None

def contains_word(sentence,list_to_check):
    words = sentence.split()
    return any(word in words for word in set(list_to_check))


def combine_unique_words(row):
    words1 = set(row['llt_term'].split())
    words2 = row['pt_term'].split()
    unique_words2 = [word for word in words2 if word not in words1]
    return row['llt_term'] + ' ' + ' '.join(unique_words2)


def load_data(abb = False):
    df = pd.read_csv("data/updated_sheet.csv", index_col=0)
    df_llt = pd.read_csv("data/llt.csv", on_bad_lines='skip')
    df_update = pd.merge(df, df_llt[["llt_term"]], on="llt_term", how="left")
    if abb:
        df_updated = df_update[["llt", "llt_term","pt_term", "pt", "soc"]]
        df_updated['words'] = df_updated['llt_term'].apply(split_words)
        words_to_check =df_updated[df_updated['words'].notna()]['words'].tolist()
        unwanted_words = ['and', 'of', 'in', 'or', 'to', '&', 'not', 'by', 'NOS','III', 'I', 'II', 'IV', 'V', 'VI','XI', 'ICU', 'XI', ]
        words_to_check = [[word for word in sublist if word not in unwanted_words] for sublist in words_to_check]
        words_to_check = [sublist for sublist in words_to_check if sublist]
        flattened = [word for sublist in words_to_check for word in sublist]
        flattened_to_check = [word for word in flattened if word == word.upper() and not word.isdigit()]
        filtered_df = df_updated[df_updated['llt_term'].apply(lambda x: contains_word(x, flattened_to_check))]
        filtered_df['llt_term'] = filtered_df.apply(combine_unique_words, axis=1)
        df_updated.update(filtered_df[['llt', 'llt_term', 'pt_term', 'pt', 'soc', 'words']])
        df_updated = df_updated[["llt", "llt_term", "pt", "soc"]]
    else:
        df_updated = df_update[["llt", "llt_term", "pt", "soc"]]
    return df_updated


def remove_special_characters(string):
    return re.sub(r"[^a-z ]", "", string)


def preprocess_data(df, df_new):
    label_encoder = LabelEncoder()
    df_train = df_new.copy()
    df_test = df.copy()

    df_train["soc"] = label_encoder.fit_transform(df_train["soc"])
    df_test["soc"] = label_encoder.transform(df_test["soc"])

    df_train["pt"] = label_encoder.fit_transform(df_train["pt"])
    df_test["pt"] = label_encoder.transform(df_test["pt"])
    df_train["llt"] = label_encoder.fit_transform(df_train["llt"])
    df_test["llt"] = label_encoder.transform(df_test["llt"])

    df_train["llt_term"] = df_train["llt_term"].apply(lambda x: str.lower(x))
    df_test["llt_term"] = df_test["llt_term"].apply(lambda x: str.lower(x))

    df_train["llt_term"] = df_train["llt_term"].apply(
        lambda x: remove_special_characters(x)
    )
    df_test["llt_term"] = df_test["llt_term"].apply(
        lambda x: remove_special_characters(x)
    )

    return df_train, df_test


def data_augmentation(df, k, sample_frac=1.0):
    def shuffle_row(row):
        nonlocal k
        new_rows = []
        for _ in range(k):
            if len(row["llt_term"].split()) == 2:
                shuffled_llt_term = " ".join(row["llt_term"].split()[::-1])
            else:
                shuffled_llt_term = " ".join(
                    np.random.permutation(row["llt_term"].split())
                )
            new_row = {
                "llt_term": shuffled_llt_term,
                "llt": row["llt"],
                "pt": row["pt"],
                "soc": row["soc"],
            }
            new_rows.append(new_row)
        return new_rows

    df_copy = df.copy()
    sample = df_copy.sample(frac=sample_frac)
    new_rows = sample.apply(shuffle_row, axis=1)
    augmented_df = pd.DataFrame(
        [row for sublist in new_rows for row in sublist], columns=df_copy.columns
    )

    return augmented_df

