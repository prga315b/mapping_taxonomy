from tqdm import tqdm
import torch
from models import SOCClassifier, PTClassifier, LLTClassifier
from training_parameters import training_init
from project_init import device
import logging


def soc_train(soc_model,epochs, data_loader_train, label_weight):
    loss_fn, optim = training_init(soc_model, label_weight=label_weight)
                                           
    soc_model.to(device)
    logging.info(f'model_train scrip soc model is sent to {device}')
    soc_model.train()
    for epoch in tqdm(range(epochs)):
        for batch in data_loader_train:
            optim.zero_grad()
            input_ids = batch["input_ids"]
            attention_mask = batch["attention_mask"]
            token_type_ids = batch["token_type_ids"]
            input_ids = torch.squeeze(input_ids, (1))
            attention_mask = torch.squeeze(attention_mask, (1))
            token_type_ids = torch.squeeze(token_type_ids, (1))
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            token_type_ids = token_type_ids.to(device)
            soc_labels = batch['soc_label'].to(device)
            soc_model_output, soc_outputs, pooled_output = soc_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)
            loss_soc = loss_fn(soc_outputs, soc_labels)
            loss_soc.backward()
            optim.step()
        logging.info(f'SOC LOSS: {loss_soc} for the epoch: {epoch}')
            
    return soc_model

def pt_train(pt_model,soc_model, epochs, data_loader_train, label_weight):

    loss_fn, optim = training_init(pt_model, label_weight=label_weight)
    logging.info(f'model_train scrip pt model is sent to {device}')

    pt_model = pt_model.to(device)
#     soc_model = soc_model.to(device)
    soc_model.eval()
    pt_model.train()
    correct_predictions_pt = 0
    for epoch in tqdm(range(epochs)):
        for batch in (data_loader_train):
            optim.zero_grad()
            input_ids = batch["input_ids"]
            attention_mask = batch["attention_mask"]
            token_type_ids = batch["token_type_ids"]
            input_ids = torch.squeeze(input_ids, (1))
            attention_mask = torch.squeeze(attention_mask, (1))
            token_type_ids = torch.squeeze(token_type_ids, (1))
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            token_type_ids = token_type_ids.to(device)
            pt_labels = batch['pt_label'].to(device)
            soc_model_output, soc_outputs, pooled_output_soc = soc_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)
            pt_model_output, pt_outputs, pooled_output_pt = pt_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids, soc_logits=pooled_output_soc)
            idx, pred_pt = torch.max(pt_outputs, dim=1)
            correct_predictions_pt += torch.sum(pred_pt == pt_labels)
            loss_pt = loss_fn(pt_outputs, pt_labels)
            loss_pt.backward()
            optim.step()
        logging.info(f'PT LOSS: {loss_pt} for the epoch: {epoch}')
    logging.info(f'predicted {correct_predictions_pt} PT labels correctly in total')
    return pt_model

def llt_train(llt_model, pt_model, soc_model, epochs, data_loader_train,label_weight):
    logging.info(f'model_train script llt model is sent to {device}')

    loss_fn, optim = training_init(llt_model, label_weight=label_weight)
    llt_model = llt_model.to(device)

    soc_model.eval()
    pt_model.eval()
    llt_model.train()
    correct_predictions_llt = 0
    for epoch in tqdm(range(epochs)):

        for batch in (data_loader_train):
            optim.zero_grad()
            input_ids = batch["input_ids"]
            attention_mask = batch["attention_mask"]
            token_type_ids = batch["token_type_ids"]
            input_ids = torch.squeeze(input_ids, (1))
            attention_mask = torch.squeeze(attention_mask, (1))
            token_type_ids = torch.squeeze(token_type_ids, (1))
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            token_type_ids = token_type_ids.to(device)
            llt_labels = batch['llt_label'].to(device)
            soc_model_output, soc_outputs, pooled_output_soc = soc_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids)
            pt_model_output, pt_outputs, pooled_output_pt = pt_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids, soc_logits=pooled_output_soc)
            llt_model_output, llt_outputs = llt_model(input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids, soc_logits=pooled_output_soc, pt_logits=pooled_output_pt)
            idx, pred_llt = torch.max(llt_outputs, dim=1)
            correct_predictions_llt += torch.sum(pred_llt == llt_labels)
            loss_llt = loss_fn(llt_outputs, llt_labels)
            loss_llt.backward()
            optim.step()
        logging.info(f'LLT LOSS: {loss_llt} for the epoch: {epoch}')
    logging.info(f'predicted {correct_predictions_llt} LLT labels correctly in total')

    return llt_model