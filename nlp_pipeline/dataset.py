import torch
from torch.utils.data import Dataset
from data_init import load_data, data_augmentation, preprocess_data
from sklearn import preprocessing
from collections import Counter
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

df = load_data(abb=True)
new_df = data_augmentation(df, k=3, sample_frac=1.0)
combined_df = pd.concat([df, new_df], axis=0).reset_index(drop=True)
df_train_raw, df_test_raw =train_test_split(combined_df, test_size=.3, random_state=None, stratify=combined_df['llt'])

                                                                                                          
df_train, df_test = preprocess_data(df, new_df)

# def df_label_encode(df_train, df_test):
#     label_encoder = preprocessing.LabelEncoder()

#     df_train['soc']= label_encoder.fit_transform(df_train['soc'])
#     df_test['soc']= label_encoder.transform(df_test['soc'])

#     df_train['pt']= label_encoder.fit_transform(df_train['pt'])
#     df_test['pt']= label_encoder.transform(df_test['pt'])
#     df_train['llt']= label_encoder.fit_transform(df_train['llt'])
#     df_test['llt']= label_encoder.transform(df_test['llt'])
#     return df_train, df_test


# df_train, df_test  = df_label_encode(df_train_raw, df_test_raw)  #check


num_pt_labels = df_train.pt.nunique()
num_soc_labels = df_train.soc.nunique()
num_llt_labels = df_train.llt.nunique()


def calculate_weights(label_list, num_labels):
    class_frequencies_soc = Counter(label_list)
    class_frequencies_soc = [class_frequencies_soc[i] for i in range(num_labels)]
    class_frequencies_soc = np.array(class_frequencies_soc) / len(label_list)
    return class_frequencies_soc
soc_label_weight = calculate_weights(df_train.soc.values, num_soc_labels)
pt_label_weight = calculate_weights(df_train.pt.values, num_pt_labels)
llt_label_weight = calculate_weights(df_train.llt.values, num_llt_labels)


df_train_array = df_train.to_numpy()
df_test_array = df_test.to_numpy()

class MeddraDataset(Dataset):
    def __init__(self, data, tokenizer):
        self.x = data[:, 1]
        self.labels_soc = data[:, 3]
        self.labels_pt = data[:, 2]
        self.labels_llt = data[:, 0]
        self.tokenizer = tokenizer
    
    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        item = self.x[idx]
        label_soc = self.labels_soc[idx]
        label_pt = self.labels_pt[idx]
        label_llt = self.labels_llt[idx]
        text_tokenized = self.tokenizer(
            item,
            max_length=30,
            add_special_tokens=True,
            padding="max_length",
            truncation=True,
            return_tensors="pt",
        )
        input_ids = text_tokenized["input_ids"].flatten()
        attention_mask = text_tokenized["attention_mask"].flatten()
        token_type_ids = text_tokenized["token_type_ids"].flatten()

        return {
            "text": item,
            "llt_label": torch.tensor(label_llt, dtype=torch.long),
            "pt_label": torch.tensor(label_pt, dtype=torch.long),
            "soc_label": torch.tensor(label_soc, dtype=torch.long),
            "input_ids": torch.tensor(input_ids, dtype=torch.long),
            "attention_mask": torch.tensor(attention_mask, dtype=torch.long),
            "token_type_ids": torch.tensor(token_type_ids, dtype=torch.long),
        }

#todo notebook on tokenization cls 

