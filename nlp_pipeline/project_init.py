from transformers import AutoModel, AutoTokenizer
import torch 


#biobert
# model = AutoModel.from_pretrained('dmis-lab/biobert-large-cased-v1.1',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("dmis-lab/biobert-large-cased-v1.1")
#clinical bert 
# tokenizer = AutoTokenizer.from_pretrained("emilyalsentzer/Bio_ClinicalBERT")
# model = AutoModel.from_pretrained("emilyalsentzer/Bio_ClinicalBERT",output_hidden_states=True)
# bert
# model = AutoModel.from_pretrained('bert-base-uncased',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("bert-base-uncased")
#distillbert
# model = AutoModel.from_pretrained('distilbert-base-uncased',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("distilbert-base-uncased")
# model = AutoModel.from_pretrained('microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#bionlp/bluebert_pubmed_mimic_uncased_L-12_H-768_A-12
#microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext
#michiyasunaga/BioLinkBERT-large

#sapBERT
# model = AutoModel.from_pretrained('cambridgeltl/SapBERT-from-PubMedBERT-fulltext',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("cambridgeltl/SapBERT-from-PubMedBERT-fulltext")
#biosyn
# model = AutoModel.from_pretrained('dmis-lab/biosyn-biobert-ncbi-disease',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("dmis-lab/biosyn-biobert-ncbi-disease")
#blueBERT
model = AutoModel.from_pretrained('bionlp/bluebert_pubmed_mimic_uncased_L-12_H-768_A-12',output_hidden_states=True)
tokenizer =  AutoTokenizer.from_pretrained("bionlp/bluebert_pubmed_mimic_uncased_L-12_H-768_A-12")
#biolink
# model = AutoModel.from_pretrained('michiyasunaga/BioLinkBERT-base',output_hidden_states=True)
# tokenizer =  AutoTokenizer.from_pretrained("michiyasunaga/BioLinkBERT-base")